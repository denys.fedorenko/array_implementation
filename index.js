function MyArray() {
    this.length = 0;

    for (let i = 0; i < arguments.length; i++) {
        this[this.length++] = arguments[i];
    }
}

MyArray.prototype.push = function (el) {
    for (let el in arguments) {
        this[this.length++] = arguments[el];
    }
    return this.length;
}

MyArray.prototype.pop = function () {
    let del = this[this.length - 1];
    delete this[this.length - 1];
    this.length--;
    return del;
}

MyArray.from = function (arr) {
    const instance = new MyArray();
    for (let i = 0; i < arr.length; i++) {
        instance.push(arr[i]);
    }
    return instance;
};

MyArray.prototype.map = function (callback) {
    let newArr = new MyArray();
    for (let i = 0; i < this.length; i++) {
        newArr[i] = callback(this[i], i, this);
    };
    return newArr;
};

MyArray.prototype.forEach = function (callback) {
    for (let i = 0; i < this.length; i++) {
        if (this[i]) {
            callback(this[i], i, this);
        }
    }
}

MyArray.prototype.reduce = function (callback, initialValue = 0) {
    for (let i = 0; i < this.length; i++) {
        initialValue = callback(initialValue, this[i], i, this);
    }
    return initialValue;
};

MyArray.prototype.filter = function (callback) {
    let instance = new MyArray();
    for (let i = 0; i < this.length; i++) {
        if (callback(this[i], i, this)) {
            instance[instance.length++] = this[i];
        }
    }
    return instance;
}

MyArray.prototype.mySort = function (callback) {
    for (let i = 0; i < this.length; i++) {
        for (let j = 0; j < this.length; j++) {
            let curVal = this[j];
            let lastVal = this[j - 1]
            if (callback(curVal, lastVal) > 0) {
                lastVal = this[j];
                curVal = this[j - 1]
            }
        }
    }
    return this
}


MyArray.prototype.ToString = function () {
    let str = this[0];
    for (i = 0; i < this.length; i++) {
        str += this[i] + ','
    }
}